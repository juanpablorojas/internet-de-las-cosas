var fecha, desde, temperatura, veces, animacion, titulo;


window.onload = function(){
	animacion = new TimelineMax();
	titulo = $("#titulo");
	animacion.from(titulo,3,{top:"-50%", ease:Elastic.easeOut});
	animacion.from(titulo,2,{rotation:360}, +2);
	fecha = $("#time");
	desde = $("#since");
	veces = $("#hits");
	temperatura = $("#temperature");
	desde.load('/arduino/since');
}
	
function newTemp(){
	temperatura[0].style.cursor = "progress";
	temperatura.load('/arduino/temperature');
	veces.load('/arduino/hits');
}