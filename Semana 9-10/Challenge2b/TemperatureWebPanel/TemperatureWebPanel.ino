/*
TemperatureWebPanel para Arduino Yun

Juan Pablo Rojas V.
Nicolás Aquiles Durango G.

Programa ligeramente modificado para entregar información con diferentes comandos, 
y distribuir elementos en la página en diferente posición.
 */

#include <Bridge.h>
#include <YunServer.h>
#include <YunClient.h>

// Listen on default port 5555, the webserver on the Yún
// will forward there all the HTTP requests for us.
YunServer server;
String startString;
long hits = 0;

void setup() {
  Serial.begin(9600);

  // Bridge startup
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  Bridge.begin();
  digitalWrite(13, HIGH);

  // using A0 and A2 as vcc and gnd for the TMP36 sensor:
  pinMode(A0, OUTPUT);
  pinMode(A2, OUTPUT);
  digitalWrite(A0, HIGH);
  digitalWrite(A2, LOW);

  // Listen for incoming connection only from localhost
  // (no one from the external network could connect)
  server.listenOnLocalhost();
  server.begin();

  // get the time that this sketch started:
  Process startTime;
  startTime.runShellCommand("date");
  while (startTime.available()) {
    char c = startTime.read();
    startString += c;
  }
}

void loop() {
  // Get clients coming from server
  YunClient client = server.accept();

  // There is a new client?
  if (client) {
    // read the command
    String command = client.readString();
    command.trim();        //kill whitespace
    Serial.println(command);
    // is "time" command?
    if (command == "time") {

      // get the time from the server:
      Process time;
      time.runShellCommand("date");
      String timeString = "";
      while (time.available()) {
        char c = time.read();
        timeString += c;
      }
      Serial.println(timeString);
      client.println("El tiempo actual es:");
      client.println(timeString);
      
    }
    
    //comando temperature
    if(command == "temperature"){
      hits++;
      int sensorValue = analogRead(A1);
      // convert the reading to millivolts:
      float voltage = sensorValue *  (5000 / 1024);
      // convert the millivolts to temperature celsius:
      float temperature = (voltage - 500) / 10;
      
      client.print("La temperatura actual es: ");
      client.print(temperature);
      client.print(" °C");

    }
    
    //comando since
    if(command == "since"){
      client.print("El programa se ejecutó desde: ");
      client.print(startString);
    }
    
    //comando hits
    if(command == "hits"){
      client.print("Se ha mostrado la temperatura ");
      client.print(hits);
      client.print(" veces");
    }

    // Close connection and free resources.
    client.stop();
  }

  delay(50); // Poll every 50ms
}



