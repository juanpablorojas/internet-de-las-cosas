/*
  Arduino Yún Bridge example

 This example for the Arduino Yún shows how to use the
 Bridge library to access the digital and analog pins
 on the board through REST calls. It demonstrates how
 you can create your own API when using REST style
 calls through the browser.

 Possible commands created in this shetch:

 * "/arduino/pulsador/7"     -> digitalRead(7)

 This example code is part of the public domain

 http://www.arduino.cc/en/Tutorial/Bridge

 */

#include <Bridge.h>
#include <YunServer.h>
#include <YunClient.h>

// Listen to the default port 5555, the Yún webserver
// will forward there all the HTTP requests you send
YunServer server;

void setup() {
  // Bridge startup
//  pinMode(13, OUTPUT);
//  digitalWrite(13, LOW);
  Bridge.begin();
//  digitalWrite(13, HIGH);

  // Listen for incoming connection only from localhost
  // (no one from the external network could connect)
  server.listenOnLocalhost();
  server.begin();
}

void loop() {
  // Get clients coming from server
  YunClient client = server.accept();

  // There is a new client?
  if (client) {
    // Process request
    process(client);

    // Close connection and free resources.
    client.stop();
  }

  delay(50); // Poll every 50ms
}

void process(YunClient client) {
  // read the command
  String command = client.readStringUntil('/');

  // is "pulsador" command?
  if (command == "pulsador") {
    pulsador(client);
  }
}

void pulsador(YunClient client) {
  int pin, value;

  // Read pin number
  pin = client.parseInt();
  
  // El comando es: "/pulsador/7"  Se puede designar el pin que se quiera. En este caso el comando está con el pin 7 (Depende donde se ponga el cable)
//  if (client.read() == '/') {
    pinMode(pin, INPUT);
    value = digitalRead(pin);

  // Send feedback to client
  client.print(F("Pulsador D"));
  client.print(pin);
  client.print(F(" cambia a  "));

  if(value==1){
//   El pulsador está encendido
     client.println("encendido");
  }
   else{
//     El pulsador está apagado
     client.println("apagado");
   }

  // Update datastore key with the current pin value
  String key = "D";
  key += pin;
  Bridge.put(key, String(value));
}
