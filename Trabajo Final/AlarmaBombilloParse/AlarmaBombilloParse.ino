//Aplicación final de Internet de las Cosas
//Aplicación de Alarma para un bombillo (simulado por el led del arduino) que permite programar a la hora que se enciente o apaga un bombillo
//Ya sea para que funcione como despertador lumínico, o para que se apague justo a la hora en que la persona se va a dormir o no lo va a utilizar
//Desde la aplicación también se puede conocer el estado actual del bombillo, en caso tal que queramos encenderlo o apagarlo a distancia
//en un momento determinado

//En este programa no se crea el objeto en Parse que controla el estado del bombillo, porque entonces cada que se ejecute el programa, se crearía el objeto
//por lo tanto, se ejecuta el programa en el Arduino, asumiendo que el objeto en Parse ya existe y está bien creado

//Integrantes: Juan Pablo Rojas - Nicolás Durango

#include <Bridge.h>
#include <Parse.h>

void setup() {
  // put your setup code here, to run once:
  Bridge.begin();
  Serial.begin(115200);
  while(!Serial);

  Serial.println("Parse Project");

  Parse.begin("4ESdrMy7MSnrPk0vN7SThnnilJCMdjNZan4et8Co", "82vMqesdSVH4XHe7P2WH2Kx3TmzZ6DVA2piouVZB");
  
  Parse.startPushService();
  Serial.print("Push Installation ID:");
  Serial.println(Parse.getInstallationId());
}

void loop() {
  // put your main code here, to run repeatedly:
  while(Parse.pushAvailable()){
    
    ParsePush push = Parse.nextPush();

    String message = push.getJSONBody();
    
    String command = push.getString("alert");
    if(command == "Encender"){  //Notificación que enciende bombillo (Que proviene de un Spinner de la App móvil indicando si quiere encenderlo o apagarlo)
      digitalWrite(13, HIGH);
      Serial.println("Se ha encendido el bombillo");
      ParseObjectUpdate update;
      update.setClassName("LightState"); //Clase en Parse que contiene el estado del bombillo
      update.setObjectId("CcGeEYkH1i");
      update.add("state", 1); //Se cambia a uno cuando se enciende el bombillo
      update.send();
      
    }else{  //Notificación que apaga bombillo (Que proviene de un Spinner de la App móvil indicando si quiere encenderlo o apagarlo)
      digitalWrite(13, LOW);
      Serial.println("Se ha apagado el bombillo");
      ParseObjectUpdate update;
      update.setClassName("LightState"); //Clase en Parse que contiene el estado del bombillo
      update.setObjectId("CcGeEYkH1i");
      update.add("state", 0); //Se cambia a cero cuando se apaga el bombillo
      update.send();
    }
  }
}
