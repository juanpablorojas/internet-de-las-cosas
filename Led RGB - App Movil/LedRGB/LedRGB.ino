/*
Ejemplo LED RGB Cátodo Común
*/
 
int redPin = 6;
int greenPin = 5;
int bluePin = 3;
int red, green, blue;
 
void setup()
{
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT); 
  Serial.begin(9600);
  
}
 
void loop()
{ 
  
  int data;
  
  //Funciona poniendo newline en el monitor
  if(Serial.available() > 0){
    red = (int)Serial.parseInt();
    green = (int)Serial.parseInt();
    blue = (int)Serial.parseInt();   
    if(Serial.read() == '\n') setColor(red, green, blue); //Por esto el new line, para que envíe \n al final y no se apague
    
  }

}
 
void setColor(int red, int green, int blue)
{
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue); 
}
