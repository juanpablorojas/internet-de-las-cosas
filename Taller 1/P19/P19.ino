/*
  Explicación de cada una de las líneas del código
*/

void setup() {
  // put your setup code here, to run once:
  int k; //Entero k---------
  Serial.begin(9600);//----Inicio de comunicación por el monitor Serial------
  
  for(k=0, Serial.print("Exp1 k = "), Serial.println(k); //Expresion 1: -----En esta expresión se inicializa k en 0. En la sentencia también se pueden poner otras cosas (En este caso se imprime el valor de k también (Como Exp1) pero esto solo ocurre una única vez en el ciclo, ya que es la expresión de inicialización)
    Serial.print("Exp2 k = "), Serial.println(k), k<10; //Expresion 2: ------Esta es la expresión de evaluación. Así que la condición es si k es menor a 10 (k<10). También se imprime en pantalla el valor de k en ese punto como Exp2
    k++, Serial.print("Exp3 k = "), Serial.println(k)){ //Expresion 3: -----Por último, esta expresión es para la "iteración" de la variable o variables. En este caso se aumenta k en 1 (k++), también se imprime el valor de k como Exp3
    
    Serial.print("In loop body, k squared = "); //for loop statement body -----Al evaluar lo anterior, se imprime el valor de k al cuadrado (k*k)
    Serial.println(k*k);
    delay(1000);
  }
}
/*
  El flujo del ciclo es el siguiente: Se inicia mostrando Exp1 k = 0 (Esto solo ocurre una única vez en todo el ciclo. Seguidamente se evalúa la condición (¿Es k menor a 10?)
  Como en esta condición hay expresiones para imprimir en pantalla, entonces se imprime Exp2 k = 0. Luego pasa a hacer las tareas dentro del ciclo, en este caso se imprime
  el cuadrado de k, que para el primer caso es 0. Una vez ocurren las sentencias dentro del for, se hace el aumento o "iteración" en el ciclo, entonces se pasa a la expresión 3.
  Para la primera ocasión mostrará Exp 3 k = 1 (Ya que antes de imprimir, aumenta k++, así que imprime el 1 la primera vez). Después de ésto debe exaluarse nuevamente la condición,
  para saber si el ciclo continua o no, es por ello que luego se muestra la Exp2 con el valor que tenga k. Luego se ejecutarán las instrucciones dentro del for (si la condición se cumple)
  Así que mostrará el cuadrado de k... Y una vez más mostrará Exp3, y luego Exp 2 (aumentando la k concurrentemente según se indicó antes, y según el orden de las instrucciones).
 Así que de aquí en adelante el orden de lo que se muestra en pantalla es: Cuadrado de k (sentencias dentro del for), Exp 3 (iteración o aumento), Exp2 (Evaluación o condición),
 y así sucesivamente.
 Al final se imprimirá el cuadrado de 9*9 = 81. Acto seguido se aumentará la k a 10, y se mostrará Exp3. Luego se mostraría la condición, que sería Exp2 con k en 10.
 Como 10 no es menor que 10, allí terminaría el ciclo for, y en vista de que no hay más instrucciones, allí termina el flujo en el monitor serial, y en el programa.
*/

void loop() {
  // put your main code here, to run repeatedly:

}
