/*
  Uso de diferentes estructuras de control.
  
  Manipulación de pulsador y leds.
  
  El flujo del programa es el siguiente:
  Se lee una cadena. Cuando se pasa por el caracter a o A en la cadena, se enciende y apaga un led externo conectado al arduino (Y se notifica esto como un mensaje en el monitor serial).
  Cuando se pasa por el caracter punto, se enciende y apaga el led incorporado en el arduino (Y se notifica esto como un mensaje en el monitor serial).
  Cuando se pasa por otros caracteres, se encienden y apagan los dos leds al mismo tiempo (Y se notifica esto como un mensaje en el monitor serial).
  En cualquier momento se puede pulsar el boton, si esto se hace, se ejecuta una rutina en la que los dos leds alternan encendiendo y apagando 3 veces.
  Al final del programa se muestra las veces que se pulsó el botón.
  
  El único inconveniente de este programa es que, debido a la concurrencia de la información que se traslada, y del programa, no es posible capturar muy ágilmente cuando se pulsa el botón.
  Por eso cuando se pulse, debe hacerse un tiempo relativamente prolongado (1 segundo, mínimo) de tal manera que el programa sí lo reconozca.
  
  Este programa puede verse como una especie de "decodificador" de mensajes, que puede identificar ciertos caracteres. Podría pensarse en un decodificador de clave morse.
  
 */

int pushButton = 12;
int ledW = 8;
int ledA = 13;
char cadena[] = "Mensaje.Codificado.Para.Encender.Led.Arduino";
int i=0; //contador para for
int contadorPulsador = 0; //Contador para contar las veces que se pulsta el botón

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(pushButton, INPUT);
  pinMode(ledW, OUTPUT); //Led externo
  pinMode(ledA, OUTPUT); //Led incorporado en el Arduino, con el pin 13
}

// the loop routine runs over and over again forever:
void loop() {
  if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, se ejecuta una rutina diferente. Esta línea se repite muchas veces porque el botón se puede pulsar en cualquier momento
  
  for(i; i<sizeof(cadena); i++){
    if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, se ejecuta una rutina diferente
    
    char caracter = cadena[i]; //Se lee el caracter de la cadena en el que vaya el ciclo

    switch (caracter){
      case '.': //Si la cadena va en un punto, se enciende el led del Arduino
        Serial.println("Estoy en un punto, enciendo led 13");
        digitalWrite(ledA, HIGH);
        if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, se ejecuta una rutina diferente
        delay(1000);
        
        digitalWrite(ledA, LOW);
        if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, se ejecuta una rutina diferente
        delay(1000);     
        break;
        
      case 'a': //Si la cadena va en una 'a', se enciende el led externo
        Serial.println("Estoy en una a, enciendo led 8");
        digitalWrite(ledW, HIGH);
        if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, se ejecuta una rutina diferente
        delay(1000);
        digitalWrite(ledW, LOW);
        if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, se ejecuta una rutina diferente
        delay(1000);
        break;
        
      case 'A': //Si la cadena va en una 'A', se enciende el led externo
        Serial.println("Estoy en una A, enciendo led 8");
        digitalWrite(ledW, HIGH);
        if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, se ejecuta una rutina diferente
        delay(1000);
        
        digitalWrite(ledW, LOW);
        if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, se ejecuta una rutina diferente
        delay(1000);
        break;
        
      default: //Se ejecuta si la cadena no va ni en punto, ni en 'a' o 'A'.
        Serial.println("No estoy en a, ni en punto, enciendo ambos leds");
        digitalWrite(ledW, HIGH);
        digitalWrite(ledA, HIGH);
        if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, se ejecuta una rutina diferente
        
        delay(1000);
        digitalWrite(ledA, LOW);
        digitalWrite(ledW, LOW);
        if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, se ejecuta una rutina diferente        
        delay(1000);
      }
  }
      
  accion:  
    while(!digitalRead(pushButton)){ //Si el botón está presionado, los leds titilan simultáneamente 3 veces. Por simplicidad, se encienden y apagan manualmente (sin cilos) para no tener tantos ciclos
      digitalWrite(ledW, LOW);
      digitalWrite(ledA, HIGH);
      delay(500);
      if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, empieza de nuevo la rutina
      
      digitalWrite(ledA, LOW);
      digitalWrite(ledW, HIGH);
      delay(500);
      if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, empieza de nuevo la rutina
      
      digitalWrite(ledW, LOW);
      digitalWrite(ledA, HIGH);
      delay(500);
      if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, empieza de nuevo la rutina
      
      digitalWrite(ledA, LOW);
      digitalWrite(ledW, HIGH);
      delay(500);
      if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, empieza de nuevo la rutina
      
      digitalWrite(ledW, LOW);
      digitalWrite(ledA, HIGH);
      delay(500);
      if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, empieza de nuevo la rutina
      
      digitalWrite(ledA, LOW);
      digitalWrite(ledW, HIGH);
      delay(500);
      if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, empieza de nuevo la rutina
      
      digitalWrite(ledW, LOW);
      if(!digitalRead(pushButton)){ contadorPulsador++; goto accion; } //Si se presiona el pulsador, empieza de nuevo la rutina
      delay(500);
  }
  
  if(i==sizeof(cadena)){ //Si se terminó de leer toda la cadena...
    if(contadorPulsador>5){
      Serial.println("Presiono el boton mas de 5 veces");
      delay(2000);
    }else
      if(contadorPulsador>10){
        Serial.println("Presiono el boton mas de 10 veces");
        delay(2000);
      }else{
        Serial.println("Presiono el boton menos de 5 veces");
        delay(2000);
      }
  }
}
