/*
Explicación de cada una de las líneas del código
*/

void setup() { //------Función de inicio de parámetros, se ejecuta una única vez en el programa--------
  // put your setup code here, to run once:
  Serial.begin(9600);//----Inicio comunicación Serial a 9600 bytes/sec--------
  Serial.println("Enter a letter A - F"); //------Imprime el texto "Enter a letter A-F" en el monitor Serial-----------

}

void loop() { ///---------Ciclo infinito que se ejecuta una y otra vez en el programa----------
  // put your main code here, to run repeatedly:
  char c;//------Definición de variable c, tipo de dato char-----------
  while(true){//--------Ciblo infinito que se repite por siempre----------
    if(Serial.available() > 0){//-------Condición: Si el monitor Serial tiene una cantidad de caracteres para leer mayor a 0 (En pocas palabras, si escribieron algo en la entrada), ejecute condiciones -------
      c = Serial.read(); //------ Se lee lo que ingresó en el monitor, y se almacena en la variable C----------
      c = toupper(c);//------ Esta función convierte cualquier caracter que contenga C en mayúsculas, y se reasigna nuevamente a C, para que C solo contenga mayúsculas --------
      
      switch(c){//---- Condición switch que permite tomar diferentes caminos en el código dependiendo de lo que el usuario ingresó --------
        case 'A': //------- Si el usuario ingresa A, se ejecuta ésto -------
          Serial.println("Great job"); //----- Se imprime en el monitor "Great job"------
          break;//---- Fin de la condición A---------
        case 'B'://------- Si el usuario ingresa B, se ejecuta ésto -------
        case 'C'://------- Si el usuario ingresa C, se ejecuta ésto (En este caso tanto B como C son la misma opción)-------
          Serial.println("You passed");//----- Se imprime en el monitor "You passed"------
          break; //---- Fin de la condición B y C---------
        case 'D': //------- Si el usuario ingresa D, se ejecuta ésto -------
          Serial.println("You're on the edge"); //----- Se imprime en el monitor "You're on the edge"------
          break;//---- Fin de la condición D---------
        case 'F'://------- Si el usuario ingresa F, se ejecuta ésto -------
          Serial.println("See you again next semester."); //----- Se imprime en el monitor "See you again next semester"------
          break;//---- Fin de la condición F---------
        default: //------- Si el usuario ingresa cualquier otro tipo de caracter, se ejecuta esta opción (Si ninguna de las anteriores se cumple) -------
          Serial.println("You can't even follow instructions"); //----- Se imprime en el monitor "You can't even follow instructions"------
          break;//---- Fin de la condición por omisión---------
      }
    }
  }
}
/*------- En pocas palabras lo que hace el programa es darle al usuario opciones de la A a la F para que digite la que desee. Esa opción que digite es su nota, y en la pantalla
se refleja su resultado de calificación según lo que haya ingresado. También existe una opción por si el usuario digita una opción que no exista entre las posibilidades (A - F)
Y esto ocurre una y otra vez por siempre.*/
