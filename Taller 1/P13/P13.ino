//Programa que muestra todos los caracteres de mi nombre en el puerto serial

  char miNombre[] = "Juan Pablo";
  int i=0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);  
}

void loop() {
  // put your main code here, to run repeatedly:
  while(i<10){
    Serial.print(miNombre[i]);
    i++;
  }
  delay(500);
}
