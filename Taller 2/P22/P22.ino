/* Buscador de Libros almacenados en una biblioteca
*/
int i,j;

bool esPar(int paginas){
  return paginas%2==0;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  Serial.println("Ingrese la opcion de la consulta que desea realizar:");
  Serial.println("1. Consultar libros del autor 1 que tengan mas de 5 paginas");
  Serial.println("2. Consultar libros que sean del autor 2 o que tengan un numero par de paginas");
  Serial.println("3. Consultar libros que tengan un numero de paginas impar");  
}

void loop() {
  // put your main code here, to run repeatedly:
  
  int libros[4][2]; //Se tienen 4 libros con 2 valores: autor y número de páginas
  //La posición [n][0] es el autor (por simplicidad solo existen los autores 1 y 2), y la [n][1] es el número de páginas
  libros[0][0] = 1;
  libros[0][1] = 4;
  libros[1][0] = 2;
  libros[1][1] = 3;
  libros[2][0] = 1;
  libros[2][1] = 6;
  libros[3][0] = 2;
  libros[3][1] = 7;
  
  char c;
  while(true){
    if(Serial.available()>0){
      c = Serial.read();
      
      switch(c){
        case '1':
          Serial.println("\nLos libros encontrados son:");
          for(i=0; i<4; i++){
            if(libros[i][0] == 1 && libros[i][1] > 5){
              Serial.print("\nLibro de autor 1 con cantidad de paginas: ");
              Serial.println(libros[i][1]);
              Serial.println();
            }
          }
          break;
          
        case '2':
          Serial.println("\nLos libros encontrados son:");
          for(i=0; i<4; i++){
            if(libros[i][0] == 2 || esPar(libros[i][1])){
              Serial.print("\nLibro de autor ");
              Serial.print(libros[i][0]);
              Serial.print(" con cantidad de paginas ");
              Serial.print(libros[i][1]);
              Serial.println();          
            }
          }
          break;

        case '3':
          Serial.println("\nLos libros encontrados son:");
          for(i=0; i<4; i++){
            if(!esPar(libros[i][1])){
              Serial.print("\nLibro de autor ");
              Serial.print(libros[i][0]);
              Serial.print(" con cantidad de paginas ");
              Serial.print(libros[i][1]);  
              Serial.println();           
            }
          }
          break;
        
        default:
          Serial.print("\nHa ingresado una opcion invalida. Por favor vuelva a intentar \n");
          break;
      }
    }
  }
  
}
