//Simulador del monitoreo de una Represa con Arduino y Temboo:
//Integrantes: Juan Pablo Rojas - Nicolás Durango
//El programa simula un arduino que monitoreará el estado de una represa. Se capturan algunas características climáticas y se suben como reporte a Dropbox
//También se envia un correo de alerta cuando alguna de las variables climáticas sobrepase un umbral definido, o el nivel del agua sobrepase el sensor del arduino

#include <Bridge.h>
#include <Temboo.h>
#include "TembooAccount.h" // contains Temboo account information, as described below

int numRuns = 1;   // Execution count, so this doesn't run forever
int maxRuns = 1;   // Maximum number of times the Choreo should be executed
const String DROPBOX_APP_KEY = "sb96ntwh3w1o6rw";
const String DROPBOX_APP_SECRET = "jr13l5n2uyz1jj8";
const String DROPBOX_ACCESS_TOKEN = "hyop99mslqri6q81";
const String DROPBOX_ACCESS_TOKEN_SECRET = "0q1kewi52wtol61";

void setup() {
  Serial.begin(9600);
  
  // For debugging, wait until the serial console is connected
  delay(4000);
  while(!Serial);
  Bridge.begin();
}

void loop() {
  int pin = 7; //Pulsador que simulará el sensor del arduino que identifica si el nivel del agua sube o no
  pinMode(pin, INPUT);
  int presion;
  int pulsador = digitalRead(pin);
  if (numRuns <= maxRuns) {
    Serial.println("Running GetWeatherByAddress - Run #" + String(numRuns++));
    
    TembooChoreo GetWeatherByAddressChoreo;

    // Invoke the Temboo client
    GetWeatherByAddressChoreo.begin();

    // Set Temboo account credentials
    GetWeatherByAddressChoreo.setAccountName(TEMBOO_ACCOUNT);
    GetWeatherByAddressChoreo.setAppKeyName(TEMBOO_APP_KEY_NAME);
    GetWeatherByAddressChoreo.setAppKey(TEMBOO_APP_KEY);
    
    // Set Choreo inputs
    GetWeatherByAddressChoreo.addInput("Units", "c");
    GetWeatherByAddressChoreo.addInput("Address", "Circular 1 # 70 - 01 Medellín");
    GetWeatherByAddressChoreo.addInput("ResponseFormat", "json");
    
    // Identify the Choreo to run
    GetWeatherByAddressChoreo.setChoreo("/Library/Yahoo/Weather/GetWeatherByAddress");

    //Filtros de Temperatura, Humedad, Presion y condición en texto
    GetWeatherByAddressChoreo.addOutputFilter("TemperaturaCelcius", "/channel/item/yweather:condition/@temp", "Response");
    GetWeatherByAddressChoreo.addOutputFilter("Humedad", "/channel/yweather:atmosphere/@humidity", "Response");
    GetWeatherByAddressChoreo.addOutputFilter("Presion", "/channel/yweather:atmosphere/@pressure", "Response");
    GetWeatherByAddressChoreo.addOutputFilter("DescripcionCondicion", "/channel/item/yweather:condition/@text", "Response");
    
    // Run the Choreo; when results are available, print them to serial
    GetWeatherByAddressChoreo.run();
    String mensaje = "Reporte del estado climático de la represa:\n";
    
    while(GetWeatherByAddressChoreo.available()) {
      
      String nombre = GetWeatherByAddressChoreo.readStringUntil('\x1F');
      nombre.trim();
      Serial.println("--nombre---"+nombre);
      String valor = GetWeatherByAddressChoreo.readStringUntil('\x1E');
      valor.trim();
      Serial.println("---valor--"+valor);
      int val = valor.toInt();
      if(nombre == "TemperaturaCelcius"){
        mensaje+="\nTemperatura en grados Celcius: "+val;
        Serial.println("Temperatura: "+val);
      }else
        if(nombre == "Humedad"){
          mensaje+="\nHumedad: "+val;
          Serial.println("Humedad: "+val);
        }else
          if(nombre == "Presion"){
            presion = val;
            mensaje+="\nPresión Atmosférica: "+val;
            Serial.println("Presión: "+val);
          }else
            if(nombre == "DescripcionCondicion"){
              mensaje+="\nLa descripción oficial de la condición es: "+val;
              Serial.println("Descripción: "+val);
            }
    }
    GetWeatherByAddressChoreo.close();

    //Se actualiza el reporte en Dropbox (Logica para subir archivo a Dropbox):
    String base64EncodedData = base64Encode(mensaje);   
    TembooChoreo UploadFileChoreo;
    UploadFileChoreo.begin();
    UploadFileChoreo.setAccountName(TEMBOO_ACCOUNT);
    UploadFileChoreo.setAppKeyName(TEMBOO_APP_KEY_NAME);
    UploadFileChoreo.setAppKey(TEMBOO_APP_KEY);
    UploadFileChoreo.setChoreo("/Library/Dropbox/FilesAndMetadata/UploadFile");
    UploadFileChoreo.addInput("FileName", "ReporteRepresa.txt");
    UploadFileChoreo.addInput("Root","sandbox");
    UploadFileChoreo.addInput("FileContents", base64EncodedData);
    UploadFileChoreo.addInput("AppSecret", DROPBOX_APP_SECRET);
    UploadFileChoreo.addInput("AccessToken", DROPBOX_ACCESS_TOKEN);
    UploadFileChoreo.addInput("AccessTokenSecret", DROPBOX_ACCESS_TOKEN_SECRET);
    UploadFileChoreo.addInput("AppKey", DROPBOX_APP_KEY);
    UploadFileChoreo.run();
    Serial.println("Se subió reporte a Dropbox");
    UploadFileChoreo.close();

    //Si la presión atmosférica es mayor a un valor, o si el pulsador (sensor) se acciona, se envía un correo de alerta
    if(pulsador == 1 || presion>100){
    
      TembooChoreo SendEmailChoreo;
      SendEmailChoreo.begin();
      SendEmailChoreo.setAccountName(TEMBOO_ACCOUNT);
      SendEmailChoreo.setAppKeyName(TEMBOO_APP_KEY_NAME);
      SendEmailChoreo.setAppKey(TEMBOO_APP_KEY);
      String alarma = "¡¡¡ALERTA!!! El sensor de la represa se ha activado, el nivel del agua ha pasado el umbral!!\nAbra las compuertas lo más pronto posible.\n";
      SendEmailChoreo.addInput("MessageBody", alarma);
      SendEmailChoreo.addInput("Subject", "Alarma Represa");
      SendEmailChoreo.addInput("Username", "juanpablo.rojasvilla@gmail.com");
      SendEmailChoreo.addInput("Password", "xnlmxxsxuzmygdid");
      SendEmailChoreo.addInput("FromAddress", "juanpablo.rojasvilla@gmail.com");
      SendEmailChoreo.addInput("ToAddress", "el-j.p@hotmail.com");
      SendEmailChoreo.setChoreo("/Library/Google/Gmail/SendEmail");
      SendEmailChoreo.run();
      Serial.println("Se envió correo de alarma: ");
      SendEmailChoreo.close();
    }
  }

  Serial.println("Waiting...");
  delay(10000); // wait 30 seconds between GetWeatherByAddress calls
}


//Método que se requiere para codificar el mensaje en base64 y poder subirlo a Dropbox
String base64Encode(String toEncode) {
  
    // we need a Process object to send a Choreo request to Temboo
    TembooChoreo Base64EncodeChoreo;

    // invoke the Temboo client
    Base64EncodeChoreo.begin();
    
    // set Temboo account credentials
    Base64EncodeChoreo.setAccountName(TEMBOO_ACCOUNT);
    Base64EncodeChoreo.setAppKeyName(TEMBOO_APP_KEY_NAME);
    Base64EncodeChoreo.setAppKey(TEMBOO_APP_KEY);

    // identify the Temboo Library choreo to run (Utilities > Encoding > Base64Encode)
    Base64EncodeChoreo.setChoreo("/Library/Utilities/Encoding/Base64Encode");
 
     // set choreo inputs
    Base64EncodeChoreo.addInput("Text", toEncode);
    
    // run the choreo
    Base64EncodeChoreo.run();
    
    // read in the choreo results, and return the "Base64EncodedText" output value.
    // see http://www.temboo.com/arduino for more details on using choreo outputs.
    while(Base64EncodeChoreo.available()) {
      // read the name of the output item
      String name = Base64EncodeChoreo.readStringUntil('\x1F');
      name.trim();

      // read the value of the output item
      String data = Base64EncodeChoreo.readStringUntil('\x1E');
      data.trim();

      if(name == "Base64EncodedText") {
        return data;
      }
    }
}
